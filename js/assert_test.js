//===================================================================================== Block 1 assertEqual

function assertEqual(actual, expected, testName) {
    if (actual === expected) {
        console.log("passed");
    } else {
        console.log('FAILED [' + testName + '] Expected ' + '\"' +
            expected + '\"' + ', but got ' + '\"' + actual + '\"');
    }
}

function multiplyByTwo(n) {
    return n * 2;
}

var output = multiplyByTwo(2); // returns 4

assertEqual(output, 4, 'it doubles 2 to 4');

//===================================================================================== Block 2 assertArraysEqual

function assertArraysEqual(actual, expected, testName) {
    // do they have the same lenght
    var sameLenght = actual.length === expected.length
    // do they have the same values
    var sameValues = true;
    for (var i = 0; i < expected.length; i++) {
        if (actual[i] !== expected[i]) {
            sameValues = false;
            break;
        }
    }

    if (sameLenght && sameValues) {
        console.log("passed");
    } else {
        console.log('FAILED [' + testName + '] Expected ' + '\"' +
            expected + '\"' + ', but got ' + '\"' + actual + '\"');
    }
}

var expected = ['b', 'r', 'o', 'k', 'e', 'n'];

var actual = 'broken'.split('');

assertArraysEqual(actual, expected, 'splits string into array of characters');

//===================================================================================== Block 3 assertObjectsEqual

function assertObjectsEqual(actual, expected, testName) {
    // your code here
    var actual = JSON.stringify(actual);
    var expected = JSON.stringify(expected);

    if (actual === expected) {
        console.log("passed");
    } else {
        console.log('FAILED [' + testName + '] Expected ' + expected +
            ', but got ' + actual);
    }
}

function updateObject(object, firstName, lastName) {
    object.firstName = firstName;
    object.lastName = lastName;
}

var person = {
    firstName: 'Cassidy',
    lastName: 'Jacobs'
};
updateObject(person, 'Jack', 'Jacobs');

var expected = {
    firstName: 'Jack',
    lastName: 'Jacobs'
};

assertObjectsEqual(person, expected, "updates person's existing first name field");


//===================================================================================== Block 4 assertWithinRange

function assertWithinRange(low, high, actual, testName) {
    if (actual >= low && actual <= high) {
        console.log("passed");
    } else {
        console.log('FAIL [' + testName + ']' + ' \"' + actual + '\"' +
            ' not within range ' + low + ' to ' + high);
    }
}

var blackjackScore = 20;
assertWithinRange(4, 21, blackjackScore, 'blackjack score should be between 4 and 21');
// console output:
// passed

var dieRoll = 1;
assertWithinRange(1, 6, dieRoll, 'die roll should be between 1 and 6');
// console output:
// passed

//===================================================================================== Block 5

// FUNCTION DEFINITION(S)
function addOne(val) {
    return val + 1;
}

// ASSERTION FUNCTION(S) TO BE USED
function assert(condition, testName) {
    if (condition) {
        console.log('passed');
    } else {
        console.log('FAILED [' + testName + ']');
    }
}

// TESTS FOR isOne
var result1 = addOne(1);
assert(result1 === 2, 'should return result of a positive input number added to 1');

var result2 = addOne(-2);
assert(result2 === -1, 'should return result of a negative input number added to 1');

//===================================================================================== Block 6

// FUNCTION DEFINITION(S)
function square(n) {
    return n * n;
}

// ASSERTION FUNCTION(S) TO BE USED

function assertEqual(actual, expected, testName) {
    if (actual === expected) {
        console.log("passed");
    } else {
        console.log('FAILED [' + testName + '] Expected ' + '\"' +
            expected + '\"' + ', but got ' + '\"' + actual + '\"');
    }
}

// TESTS CASES

var value = square(2);

assertEqual(value, 4, " square 2 * 2 to 4");

//===================================================================================== Block 7

// Note: This is a simple, albeit temporarily incorrect implementation of the standard Array method "every()":
// https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/every

// FUNCTION DEFINITION(S)
function every(array, callbackFunction) {
    var doesEveryElementMatch = true;

    for (var i = 0; i < array.length; i++) {
        doesEveryElementMatch = callbackFunction(array[i]);
    }

    return doesEveryElementMatch;
}

// ASSERTION FUNCTION(S) TO BE USED
function assertEqual(actual, expected, testName) {
    if (actual === expected) {
        console.log("passed");
    } else {
        console.log('FAILED [' + testName + '] Expected ' + '\"' +
            expected + '\"' + ', but got ' + '\"' + actual + '\"');
    }
}

function lessThan10(val) {
    return val < 10;
}

// TESTS CASES

var arrayFalse = [1, 2, 6, 4, 7, 11];
var actualFalse = every(arrayFalse, lessThan10);

assertEqual(actualFalse, false, "should return fasle when not all array" +
    + "values return true when tested");

//===================================================================================== Block 8

// FUNCTION DEFINITION(S)
function addOne(n) {
    return n + 1;
}

function map(array, callbackFunction) {
    var newArray = [];

    for (var i = 0; i < array.length; i++) {
        newArray[i] = callbackFunction(array[i]);
    }
    return newArray;
}

// function cubeAll(numbers) {
//   return map(numbers, function(n) {
//     return n * n;
//   });
// }

function cubeAll(numbers) {
    return map(numbers, addOne);
}

// ASSERTION FUNCTION(S) TO BE USED

function assertArraysEqual(actual, expected, testName) {
    // do they have the same lenght
    var sameLenght = actual.length === expected.length
    // do they have the same values
    var sameValues = true;
    for (var i = 0; i < expected.length; i++) {
        if (actual[i] !== expected[i]) {
            sameValues = false;
            break;
        }

    }

    if (sameLenght && sameValues) {
        console.log("passed");
    } else {
        console.log('FAILED [' + testName + '] Expected ' + '\"' +
            expected + '\"' + ', but got ' + '\"' + actual + '\"');
    }
}

// TESTS CASES

var inputArray = [1, 2, 3];
var expectedArray = [2, 3, 4];
var actualArray = cubeAll(inputArray);

assertArraysEqual(actualArray, expectedArray, " add 1 numbers: ");

//===================================================================================== Block 9

// FUNCTION DEFINITION(S)
function addFullNameProp(obj) {
    var firstName = obj.firstName;
    var lastName  = obj.lastName;

    if (firstName.length !== 0 && lastName.length !== 0) {
      obj.fullName = firstName + ' ' + lastName;
    }
  
    return obj;
  }
  
  // ASSERTION FUNCTION(S) TO BE USED
  
  function assertObjectsEqual_001(actual,expected,testName){
      // your code here
      var actual   = JSON.stringify(actual);
      var expected = JSON.stringify(expected);
  
      if (actual === expected) {
          console.log("passed");
      } else {
          console.log('FAILED [' + testName + '] Expected ' + expected +
              ', but got ' + actual);
      }
  }

  // TESTS CASES

var actor = {
    firstName: "James",
    lastName:  "Bond",
    fullName:  ""
}

var expectedActor = {
    firstName: "James",
    lastName:  "Bond",
    fullName:  "James Bond"
}

var ActualActor = addFullNameProp(actor);
assertObjectsEqual_001(ActualActor,expectedActor,"IS quale Fullname: ");
