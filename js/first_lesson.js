
//=========================

console.log("START JS!")

//=========================

function showTest() {
    console.log("Data");
}

showTest();

//=========================


function showPrimes(n) {
    nextPrime: for (let i = 2; i < n; i++) {

        for (let j = 2; j < i; j++) {
            if (i % j == 0)
                continue nextPrime;


            //code
        }
        alert(i); // простое
    }
}


  // showVariant(5);

//=========================


function showVariant(text) {
    let temp = "Are you " + text + " ?";
    return confirm(temp);
}

function testName(namaisgood) {
    if(namaisgood)
        alert("COOL")
    else
        alert("You are Loser!")
}


// var is_good_name = showVariant("Nikita");

 // testName(is_good_name);

//=========================

function addNumber(a,b) {
    var res = a + b;
    console.log(res);
    return res;
}

function minusNumber(a,b) {
    var res = a - b;
    console.log(res);
    return res;
  //  return (a > b) ? (a - b) : (b - a);
}

function numberOperation(a,b) {
    var res = a / b;
    var mod = a % b;

    console.log("res = ",res);
    console.log("mod = ", mod);

    var ret = ((res > mod) ? true : false);

    console.log("Is result > module", ret);

    ret = 0;
    for(let i = 0; i < 10; i++){
        ret += i;
        console.log(ret);
    }
}


function runExample() {

    var res = addNumber(5, 3);
    if (res != 8)
        console.log("test is falil!");

    var res = minusNumber(5, 3);
    if (res != 2)
        console.log("test is falil!");

    numberOperation(8,3);
    
    console.log("All test is Good execute!")

}


runExample();

//========= Write text in HTML ========

function writeTextTest(data_input_txt) {
    document.write('<p>');
    document.write(data_input_txt);
    document.write('</p>');
}


writeTextTest("New text in p tag!");

//======== Get text from console! =====

function getConsoleTextInWindow() {

    var name ="Your name is ";
     name += prompt('What is your name?')

    alert(name);

    document.write('<h2>');
    document.write(name);
    document.write('</h2>');
}


// getConsoleTextInWindow();

//======== array show to console ========

function findSmallestNumberAnongMixedElements(arr) {
    if(!Array.isArray(arr)){
        console.log("It is not array!");
        return false;
    }
    
    if('2' == 2)
        console.log("true");

    if('2' === 2)
        console.log("true");

    if(arr.length === 0) {
        console.log("Array is empty!");
        return false;
    }

    var number = new Array();

    for(var i = 0; i < arr.length ; i ++) {
        if(typeof arr[i] === 'number')
            number.push(arr[i]);
    }

    var min_value = number[0];

    for(let i = 0; i < number.length; i++) {
        if(min_value > number[i])
            min_value = number[i];
    }

    console.log("MIN VALUE IS: " + min_value);
}


function findSmallestNumberAnongMixedElements002(arr) {
    if(!Array.isArray(arr)){
        console.log("It is not array!");
        return false;
    }

    if(arr.length === 0) {
        console.log("Array is empty!");
        return false;
    }

    var min_value;
    var is_firs_number = false;

    for(var i = 0; i < arr.length ; i ++) {
        if( typeof arr[i] === 'number') {
            if(!is_firs_number){
                min_value = arr[i];
                is_firs_number = true;
                continue;
            }
            
            if(min_value > arr[i])
                min_value = arr[i];
        }
    }

    console.log("MIN VALUE IS: " + min_value);
} 

function retObjectResultOperation(Object,new_name) {
    if(Object.name === new_name) {        
        Object.is_change_name = false
    }  else {
        Object.name = new_name;
        Object.is_change_name = true;
    }
}


function runArrayExample() {
    var tempArray = new Array();
    // empty array
    findSmallestNumberAnongMixedElements(tempArray);
    // min value is 4;
    findSmallestNumberAnongMixedElements([4,'lincoln',9,'octopus', 7]);
    // min value is 1
    findSmallestNumberAnongMixedElements002([4,'lincoln',9,'octopus', 7,1]);


    var object_person = {
        name: "Tom",
        is_change_name: false
    };

    retObjectResultOperation(object_person,"Tom");

    if(!object_person.is_change_name)
        console.log("not change name!");

    retObjectResultOperation(object_person,"Alex");

    if(object_person.is_change_name)
        console.log("Change name! to" + object_person.name);
    
}

runArrayExample();

//=======================

function sum(x, y){
    return x + y;
}
 
function subtract(x, y){
    return x - y;
}
 
function operation(x, y, func){
  
    const result = func(x, y);
    console.log(result);
}
 
console.log("Sum");
operation(10, 6, sum);  // 16
 
console.log("Subtract");
operation(10, 6, subtract); // 4

//================================

function addToArray(array, value) {
    array.push(value);
}

function addToArray2(array, value) {
    array[array.length] = value;
}

function addToArray_test() {

    var arr = new Array();

    addToArray(arr,"Nikita");
    console.log(arr);
    addToArray(arr,'Vova');
    addToArray(arr,'Vita');
    console.log(arr);
    addToArray2(arr,'Alisa');
    console.log(arr);
  
    arr.unshift("Alisa");
    console.log(...arr);

    arr.pop();
    arr.shift();
    
    console.log(arr);
    arr = [];
    console.log(arr);
}


addToArray_test();

//==============================


function writeLetter() {
    var text_str = "New York!";
    document.write('<p>');
  
    for(let i = 0 ; i < text_str.length; i++) {
        console.log(text_str.charAt(i));
        document.write(text_str.charAt(i));
    }
    document.write('</p>');

    }
writeLetter();

//==============================


function switchExample() {
    var str = " 1 - call alert!\n 2 - call console log \n 3 - call exit!";
    var value = 1;
    while(value) {
        value = prompt(str);
        switch (value) {
            case '3':
                value = 0;
                break;
            case '1': 
                alert("Alert call!!!");
                break;
            case '2':
                console.log("Console log call!!!");
                break;
            default:
                break;
        }
    }  
}


switchExample();