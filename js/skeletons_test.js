//==================================== Sckeleton ======================================

function assertEqual(actual, expected, testName) {
    if (actual === expected) {
        console.log("passed");
        return true;
    } else {
        console.log('FAILED [' + testName + '] Expected ' + '\"' +
            expected + '\"' + ', but got ' + '\"' + actual + '\"');
        return false;
    }
}

function assertArraysEqual(actual, expected, testName) {
    // do they have the same lenght
    var sameLenght = actual.length === expected.length
    // do they have the same values
    var sameValues = true;
    for (var i = 0; i < expected.length; i++) {
        if (actual[i] !== expected[i]) {
            sameValues = false;
            break;
        }
    }

    if (sameLenght && sameValues) {
        console.log("passed");
    } else {
        console.log('FAILED [' + testName + '] Expected ' + '\"' +
            expected + '\"' + ', but got ' + '\"' + actual + '\"');
    }
}

function assertObjectsEqual(actual, expected, testName) {
    // your code here
    var actual = JSON.stringify(actual);
    var expected = JSON.stringify(expected);

    if (actual === expected) {
        console.log("passed");
    } else {
        console.log('FAILED [' + testName + '] Expected ' + expected +
            ', but got ' + actual);
    }
}

//===================================================================================== Block 1 

// SOLUTION

// FUNCTION DEFINITION
function addFullNameProp(obj) {
    var firstName = obj['firstName'];
    var lastName  = obj['lastName'];
  
    if (firstName && lastName) {
      obj['fullName'] = firstName + ' ' + lastName;
    }
  
    return obj;
  }
  
  // ASSERTION FUNCTION(S) TO BE USED
//   function assertObjectsEqual(actual, expected, testName) {
//     actual = JSON.stringify(actual);
//     expected = JSON.stringify(expected);
  
//     if ( actual === expected ) {
//       console.log('passed');
//     } else {
//       console.log('FAILED [' + testName + '], expected "' + expected + '", but got "' + actual + '"')
//     }
//   }
  
  // TESTS FOR ADD FULL NAME PROP
  var person = {
    firstName: 'Chris',
    lastName: 'Riccolo'
  };
  var actualResult1 = addFullNameProp(person);
  var expectedResult1 = {
    firstName: 'Chris',
    lastName: 'Riccolo',
    fullName: 'Chris Riccolo'
  };
  assertObjectsEqual(actualResult1, expectedResult1, 'should add fullName property when firstName and lastName are defined');
  
  var missingValues1 = {
    firstName: 'Chris'
  };
  var actualMissing1 = addFullNameProp(missingValues1);
  var expectedMissing1 = {
    firstName: 'Chris'
  };
  assertObjectsEqual(actualMissing1, expectedMissing1, 'should not add fullName property when lastName is undefined');
  
  var missingValues2 = {
    lastName: 'Riccolo'
  };
  var actualMissing2 = addFullNameProp(missingValues2);
  var expectedMissing2 = {
    lastName: 'Riccolo'
  };
  assertObjectsEqual(actualMissing2, expectedMissing2, 'should not add fullName property when firstName is undefined');

// =================

// SKELETON

// FUNCTION DEFINITION
function addFullNameProp(obj) {
    // set variable equal to firstName property within input object
    // set variable equal to lastName property within input object
    // if firstName and lastName are both defined
      // add a property to input object - key: fullName, value firstName and lastName with space in between
  
    // return input object
}
  
  // ASSERTION FUNCTION(S) TO BE USED
  // function assertObjectsEqual(actual, expected, testName) {
     // Write assertObjectsEqual because function returns an object
  // }
  
  // TESTS FOR ADD FULL NAME PROP
  // Test object with both first and last name is defined in input object
  // Test object with last name missing
  // Test object with first name missing

//===================================================================================== Block 2 

// Skeleton

// FUNCTION DEFINITION(S)
 
function sum(numbers) {
    // returns the sum of an array of numbers
    var sum = 0;
    for(var i = 0; i < numbers.length; i++){
        sum += numbers[i];
    }
    return sum;
}

function average(numbers) {
    // uses sum function
    // returns the average of an array of numbers
    return (sum(numbers)/numbers.length);
}

// ASSERTION FUNCTION(S) TO BE USED
  
// TESTS CASES
var inputArray = [1,2,3];
var expectedAverage = 2; // (1 + 2 + 3)/3 = 2
var actualAverage = average(inputArray);

assertEqual(actualAverage,expectedAverage," Test if averange is true: ");

//===================================================================================== Block 3 

// FUNCTION DEFINITION(S)

// USE THIS FUNCTION TO GENERATE A RANDOM NUMBER
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function decorateClassListWithAges(classList) {
    // creates an object for each string in the input array, with an age of 10 or 11
    var ObjectArray = []; 
    
    for(var i = 0; i < classList.length; i++) {
        var ObjectOne = {};
        ObjectOne['name'] = classList[i];
        ObjectOne["age"]  = getRandomIntInclusive(10,11);
        ObjectArray[i] = ObjectOne; 
    }

    // returns an array of these objects
    return ObjectArray;
}
  
// ASSERTION FUNCTION(S) TO BE USED

function assertMinMaxRange(inputNumbers, min,max) {
    if (inputNumbers >= min && inputNumbers <= max) {
       return true;
    } else {
      return false;
    }
}

function assertRangeArrays(arrayList,testName) {
    var isRangeGood = true;
    var failIndex = 0;
    for(var i = 0; i < arrayList.length; i++) {
        if(!assertMinMaxRange(arrayList[i]['age'],10,11)) {
            isRangeGood = false;
            failIndex = i;
            break;
        }
    }

    if(isRangeGood) {
        console.log("passed");
    } else {
        console.log('FAILED [' + testName + ']' + " age: " + arrayList[failIndex]['age'] + ' is not in range!');
    }
}

// TESTS CASES

// Sample Input
var classList = ["Joe", "Jack", "John", "Fred", "Frank", "Barry", "Larry", "Mary",
"Harry", "Farrell", "Susan", "Monica", "Keira", "Caroline", "Harriet", "Erica",
"Luann", "Cheryl", "Beth", "Rupa", "Linda", "Allison", "Nancy", "Dora"];

// Sample Output
var classListWithAges = [{"name":"Joe","age":11},{"name":"Jack","age":10},
{"name":"John","age":11},{"name":"Fred","age":11},{"name":"Frank","age":11},
{"name":"Barry","age":11},{"name":"Larry","age":11},{"name":"Mary","age":11},
{"name":"Harry","age":11},{"name":"Farrell","age":10},{"name":"Susan","age":10},
{"name":"Monica","age":11},{"name":"Keira","age":10},{"name":"Caroline","age":10},
{"name":"Harriet","age":11},{"name":"Erica","age":11},{"name":"Luann","age":10},
{"name":"Cheryl","age":11},{"name":"Beth","age":10},{"name":"Rupa","age":11},
{"name":"Linda","age":10},{"name":"Allison","age":10},{"name":"Nancy","age":10},
{"name":"Dora","age":10}];

var valClassList = decorateClassListWithAges(classList);
assertRangeArrays(valClassList,"Change classList array and add random age TEST: ");

//===================================================================================== Block 4 

// Skeleton

// FUNCTION DEFINITION(S)
function isIsogram(text) {
    // empty string should return true
    if(text == "") {
        return true;
    }
    text = text.toLowerCase();
    var arrayOfLetters = text.split("");
    // add each char to a set
    var setOfLetters = new Set(arrayOfLetters);
    // note: a set drops dup values
    // thus, to see if all the chars were unique,
    // check length of text and the size of the set
   return setOfLetters.size === arrayOfLetters.length;

}
  
// ASSERTION FUNCTION(S) TO BE USED
    // exist now go to up!
// TESTS CASES

const set1 = new Set([1,2,3,4,5]);
console.log(set1.has(1));
console.log(set1.has(4));
console.log(set1.has(7));

var inputPass_1 = "abcdefg";
var actualPass_1 = isIsogram(inputPass_1);
assertEqual(actualPass_1,true,"shoud return true for an isogram with all lower case");

var inputPass_2 = "sToP";
var actualPass_2 = isIsogram(inputPass_2);
assertEqual(actualPass_2,true,"shoud return true for an isogram with mixid case");

var inputFail_1 = "stopped";
var actualFail_1 = isIsogram(inputFail_1);
assertEqual(actualFail_1,false,"shoud return false for an non-isogram with lower case");

var inputFail_2 = "aAqzrj";
var actualFail_2 = isIsogram(inputFail_2);
assertEqual(actualFail_2,false,"shoud return false for an non-isogram with mixed case");

//===================================================================================== Block 5 

// Skeleton

// FUNCTION DEFINITION(S)
function findMaxRepeatCountInWord(word) {
    // Break up individual word into individual letters.
    var letters = word.split("");
    // Count the instances of each letter
    var letterCount = {};
    for(var i = 0; i < letters.length; i++) {
        var currentLetter = letters[i];
        if(letterCount[currentLetter] === undefined) {
            letterCount[currentLetter] = 1;
        } else {
            letterCount[currentLetter]++;
        }
    }
    // Iterate all the counts and find the highest
    var max = 0;
    for(var letter in letterCount) {
        if(letterCount[letter] > max) {
            max = letterCount[letter];
        }
    }
    // Return this word's max repeat count
    return max;
  }
  
  function findFirstWordWithMostRepeatedChars(text) {
    var maxRepeatCountOverall = 0;
    var wordWithMaxRepeatCount = '';
  
    // Break up input text into words (space-delimited).
    var words = text.split(" ");
    // For each word...
    for(var j = 0; j < words.length; j++) {
      var word = words[j];
      var repeatCountForWord = findMaxRepeatCountInWord(word)
      // If that max repeat count is higher than the overall max repeat count, then
      // max repeat count, then 
      if(repeatCountForWord > maxRepeatCountOverall) {
         // update maxRepeatCountOverall
          maxRepeatCountOverall = repeatCountForWord;
          // update wordWithMaxRepeatCount
          wordWithMaxRepeatCount = word;
      }
    }
    return wordWithMaxRepeatCount;
  }
  
  // ASSERTION FUNCTION(S) TO BE USED
  
  // TESTS CASES

  var input_1 = "passed";
  var actual_1 = findMaxRepeatCountInWord(input_1);
  var expected_1 = 2;
  assertEqual(actual_1,expected_1,"shoud return 2 for given word");


  var input_2 = "I passed my exam ary you not entertained";
  var actual_2 = findMaxRepeatCountInWord(input_2);
  var expected_2 = "passed";
  assertEqual(actual_2,expected_2,"should return word with most repeated characters");

//===================================================================================== Block 6 Render Phone Number.

// this is a constructor function, we will use it to create new instances of our cars
function Car(color, type) {
    this.color = color;
    this.type = type;
    this.gas = 12;
  }
  
  // below are methods that we have attached to the car's PROTOTYPE chain
  Car.prototype.drive = function() {
    this.gas -= 1;
  }
  
  Car.prototype.paintJob = function(color) {
    this.color = color;
  }

  // now we can generate a new instance of Car using the constructor function as follows:
var myCar = new Car("blue", "sedan");
console.log('myCar:', myCar);
myCar.drive();
console.log('myCar:', myCar);


//======================================


// Skeleton

// FUNCTION DEFINITION(S)
function PhoneNumberFormatter(numbers) {
    this.numbers = numbers;
    var local_temp_value = function name() {
        return 2;
    };
  }
  
  PhoneNumberFormatter.prototype.render = function() {
    var string = '';
    // your code here
    // get area code, wrap in parentheses add to string
    string += this.parenthesize(this.getAreaCode());
    // is valid country number ?
    string += " ";
    // get exchange code, add to string
    string += this.getExchangeCode();
    // add hyphen to string
    string += "-";
    // get line number, add to string
    string += this.getLineNumber();

    //console.log(this.local_temp_value);
    return string;
  };
  
  PhoneNumberFormatter.prototype.getAreaCode = function() {
    // your code here
    return this.slice(0,3);
  };
  
  PhoneNumberFormatter.prototype.getExchangeCode = function() {
    // your code here
    return this.slice(3,6);
  };
  
  PhoneNumberFormatter.prototype.getLineNumber = function() {
    // your code here
    return this.slice(6,10);
  };
  
  PhoneNumberFormatter.prototype.parenthesize = function(string) {
    return '(' + string + ')';
  };
  
  PhoneNumberFormatter.prototype.slice = function(start, end) {
    return this.numbers.slice(start, end).join('');
  };
  
// ASSERTION FUNCTION(S) TO BE USED
  
// TESTS CASES

// [6, 5, 0, 8, 3, 5, 9, 1, 7, 2]
// '(650) 835-9172'


var formatter = new PhoneNumberFormatter([6, 5, 0, 8, 3, 5, 9, 1, 7, 2]);
var formattedNumber = formatter.render();
console.log(formattedNumber);


//===================================================================================== Block 7 

// Skeleton

// FUNCTION DEFINITION(S)
function findLongestPalindrome(sentence) {
    // split sentence into words
    var words =  sentence.toLowerCase().split(' ');
    var polindromes = [];
    // iterate words and collect the palindromes

    for(var i = 0; i < words.length; i++) {
        if(isPalindrome(words[i])){
            polindromes.push(words[i]);
        }
    }

    // sort the list of palindromes by word length
    
    var resultPolindrome = "";
    if(polindromes.length > 0) 
        resultPolindrome = polindromes[0];

    for(var i = 0; i < polindromes.length; i++) {
        if((i + 1) < polindromes.length) {
            if(sortAscendingByLength(polindromes[i], polindromes[i + 1]) == 0)
                resultPolindrome = b;
        }
    }

    // return the largest item in the sorted list

    return resultPolindrome;
}
  
  
  function reverseString(string) {
    return string.split('').reverse().join("");
  }
  
  function isPalindrome(word) {
    // hint: you can detect palindromes by comparing a string to its reverse
    var reverseWord = reverseString(word);
    if(reverseWord === word){
        return true;
    }
    return false;
  }
  
  function sortAscendingByLength(a, b) {
    if (a.length > b.length) {
      return 1;
    } else if (a.length < b.length) {
      return -1;
    }
    return 0;
  }
  
  // ASSERTION FUNCTION(S) TO BE USED
  
  // TESTS CASES

  var Words = "string";
  var res = reverseString(Words);
  console.log(res);

  var myStrPolindrom = "hello What yor's name? Hi I'm Anna , Alla";
  var polindrom = findLongestPalindrome(myStrPolindrom);
  console.log(polindrom);

  //==============================

  var obj_test = {
    name: "nnnnn",
    age : 333,

  };


  var Rectangle = class {
    constructor(height, width) {
      this.height = height;
      this.width = width;
    }

    ddd(params) {
      console.log("Height: ", this.height);
    }
  };

  
  var Rectangle2 = class {
    constructor(height, width) {
      this.height = height;
      this.width = width;
    }

    ddd(params) {
      console.log("Height2: ", this.height);
    }

    show(text){
      console.log(text);
    }

  };


  var temp = new Rectangle(22,33);
  temp.ddd();
  temp.height = 5555;
  temp.ddd();
  

  var temp2 = new Rectangle2(22,33);
  temp2.ddd();
  temp2.height = 555;
  temp2.ddd();
  temp2.show("Anastasia");


  function fff_name(text) {
    console.log(text);
  }

  function fff_name(text) {
    console.log("double",text);
  }

  fff_name("hello");

  console.log(Rectangle.name);
  // отобразится: "Rectangle"
  
  // именованный
  var Rectangle = class Rectangle2 {
    constructor(height, width) {
      this.height = height;
      this.width = width;
    }
  };
  console.log(Rectangle.name);
  // отобразится: "Rectangle2" 

  var is_good_run_test = assertEqual(1,1,"GOOG - RUN TEST");
  if(is_good_run_test){
    console.log('OK');
  } else {
    console.log("NOT OK");
  }

  is_good_run_test = assertEqual(1,2,"GOOG - RUN TEST");
  if(is_good_run_test){
    console.log('OK');
  } else {
    console.log("NOT OK");
  }