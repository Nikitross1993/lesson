
function search_array_and_make_sqr(obj, key) {
    if(obj[key] === undefined){
        return [];
    }

    if(!Array.isArray(obj[key])){
        return [];
    }

    if(obj[key].length === 0) {
        return [];
    }

    var resultArray = [];
    for(var i = 0; i < obj[key].length; i++) {
        
        // resultArray.push((obj[key][i])*(obj[key][i]));

        var number_square = Math.pow(obj[key][i],2);
        resultArray.push(number_square);
    
    }

    return resultArray;

}


var objTemp = {
    key: [1,2,4,5]
};

var res = search_array_and_make_sqr(objTemp, "key");

console.log(res);


//==========================

function removeElement(array,discarder) {
    
   var tempArray = [];
    
    for(var i = 0; i < array.length; i++){
        if(array[i] === discarder){
            array.splice(i,1);
            //i--;
            i = 0;
        }
    }
    return array;
}

var arr  = [1,2,3,2,5];
var arr2 = [2,1,4,7,2];


removeElement(arr,2);

console.log(arr);
console.log(arr);

removeElement(arr2,2);

console.log(arr2);

